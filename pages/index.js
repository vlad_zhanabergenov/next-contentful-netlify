import { fetchEntries } from '../src/contentful'
import Post from '../src/Post'

import Head from 'next/head'

import styles from '../styles/Home.module.css'

export default function Home({ items }) {
  return (
    <div className={ styles.container }>
      <Head>
        <title>Next Contentful Netlify</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={ styles.main }>
        <p className={ styles.description }>
          <code className={styles.code}>Next Contentful Netlify test</code>
        </p>
        <div className={ styles.grid }>
          { items.map((i, n) =>
              <Post key={ `${ i.slug }_${ n }` } title={ i.title } slug={ i.slug }/>
          )}
        </div>
      </main>
    </div>
  )
}

export async function getStaticProps() {
  const res = await fetchEntries()

  const items = await res.map(entry => {
    return entry.fields
  })

  return {
    props: {
      items
    }
  }
}
