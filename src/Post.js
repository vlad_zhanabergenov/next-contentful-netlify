import styles from '../styles/Home.module.css'

function Post({ title, slug }) {
  return (
    <div className={ styles.card }>
      <h3>{ title } &rarr;</h3>
      <p>{ slug }</p>
    </div>
  )
}

export default Post
